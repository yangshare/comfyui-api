package hiforce.pixel.open.sample;

import com.alibaba.fastjson2.JSON;
import hiforce.pixel.comfy.model.node.WorkflowApi;
import hiforce.pixel.open.sample.general.GeneralPromptSample03;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;

/**
 * 本地模式调用示例
 */
public class Comfyui {

    // 格式化日期
    static String formattedDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

    public static void main(String[] args) {
        GeneralPromptSample03 sample = new GeneralPromptSample03();
        /*String prompt = "car";
        String workflowJSON = sample.getTextFromResource("/general/线框图/线框图.json");
        String workflowApiJSON = sample.getTextFromResource("/general/线框图/线框图_api.json");
        sample.init(prompt, workflowJSON, workflowApiJSON).run();*/


        //提示词
        String prompt = "Chinese style animals, Chinese style pets, Chinese style wildlife, Chinese style birds, Chinese style cats, Chinese style dogs, Chinese style dragons, Chinese style tigers, Chinese style pandas, Chinese style horses";
        //工作流
        String workflowJSON = sample.getTextFromResource("/general/花园/花园.json");
        //工作流api
        String workflowApiJSON = sample.getTextFromResource("/general/花园/花园_api.json");
        //构建工作流api对象
        WorkflowApi workflowApi = JSON.parseObject(workflowApiJSON, WorkflowApi.class);

        //循环4次，用该提示词生成4张图片
        for (int i = 0; i < 4; i++) {
            //设置随机种子
            workflowApi.setNodeFieldValue(3, "seed", seed());
            //设置提示词
            workflowApi.setNodeFieldValue(6, "text", prompt);
            //设置文件名（也就是生成图片的保存路径）
            workflowApi.setNodeFieldValue(9, "filename_prefix", formattedDate + File.separator + "LineArt");

            //初始化工作流
            sample.init(workflowJSON, workflowApi)
                    //执行工作流
                    .run();
        }
    }

    private static long seed() {
        long seed = System.currentTimeMillis();

        // 初始化随机数生成器
        Random random = new Random(seed);

        // 生成一个随机长整数作为AI绘画的随机种子
        long aiPaintingSeed = Math.abs(random.nextLong());

        // 输出生成的随机种子
        System.out.println("Generated AI painting random seed: " + aiPaintingSeed);

        return aiPaintingSeed;

    }
}